import { Message } from 'element-ui'
const Axios = require('axios')

// console.log('baseURL', process.env.VUE_APP_BASE_URL)
var Http = Axios.create({
  baseURL: process.env.VUE_APP_BASE_URL
})

Http.interceptors.request.use(function (config) {
  // console.log('config', config)
  // console.log('token', sessionStorage.getItem('token'))
  config.headers.Authorization = sessionStorage.getItem('token')
  return config
}, function (error) {
  return Promise.reject(error)
})
Http.interceptors.response.use(function (response) {
  // console.log('response', response.data)
  // console.log('typeof', typeof response.data.code)
  if (response.data.code !== '0') {
    // console.log('this', this)
    // console.log('Message', Message.error)
    Message.error({
      message: response.data.msg

    })
    return Promise.reject(response.data)
  }
  return response.data
}, function (error) {
  return Promise.reject(error)
})

export default Http
