import Vue from 'vue'
import Vuex from 'vuex'
import plugins from './plugins'
import modules from './modules'

Vue.use(Vuex)

const strict = process.env.NODE_ENV !== 'production'
console.log('strict', strict)

for (const i in modules) {
  modules[i].namespaced = true
}

export default new Vuex.Store({
  strict,
  modules: {
    ...modules
  },
  plugins
})
