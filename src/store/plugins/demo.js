export default store => {
  // 当 store 初始化后调用
  store.subscribe((mutation, state) => {
    // 每次 mutation 之后调用
    // mutation 的格式为 { type, payload }
    console.log(`%c commit > ${mutation.type}:`, 'color:#fff;background:#10a6b1d7')
    console.log(mutation.payload)
  })
}
