export default {
  locale: {
    title: 'Languages'
  },
  login: {
    title: 'Login Form',
    placeholder: {
      account: 'account',
      password: 'password'
    },
    submit: 'submit',
    rules: {
      account: 'Please enter the account',
      password: 'Please input a password'
    }

  }
}
