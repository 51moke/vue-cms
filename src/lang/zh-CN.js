export default {
  locale: {
    title: '多语言'
  },
  login: {
    title: '系统登录',
    placeholder: {
      account: '用户名',
      password: '密码'
    },
    submit: '登录',
    rules: {
      account: '请输入帐号',
      password: '请输入密码'
    }
  }
}
