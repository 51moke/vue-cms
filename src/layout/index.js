const Main = () => import('./MainLayout.vue')
const Login = () => import('./LoginLayout.vue')

export default { Main, Login }
