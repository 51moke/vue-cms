
import VueRouter from 'vue-router'

// 登录页
import Login from '@pages/Login'
import Home from '@pages/Home'

// 路由配置
const routes = [
  {
    path: '/',
    redirect: {
      name: 'home'
    }
  },
  {
    name: 'home',
    path: '/home',
    component: Home,
    meta: {
      title: '首页',
      frameLayout: 'Main'
    }
  },
  {
    name: 'login',
    path: '/login',
    component: Login,
    meta: {
      title: '登录页',
      frameLayout: 'Login'
    }
  },
  {
    path: '*',
    component: {
      template: '<div>404</div>'
    }
  }
]

// 路由
const router = new VueRouter({
  routes
})
router.beforeEach((to, from, next) => {
  // console.log('导航守卫', sessionStorage.getItem('token'))
  // console.log('to.name', to.name)
  if (sessionStorage.getItem('token')) {
    if (to.name === 'login') {
      // console.log('跳到home页')
      next({
        name: 'home'
      })
      return
    }
  } else {
    // console.log('打印页面', '|' + to.name + '|')
    // console.log('typeof', typeof to.name)
    // console.log('Login')
    if (to.name !== 'login') {
      // console.log('回到login页')
      next({
        name: 'login'
      })
      return
    }
    // console.log('没有登录')
  }
  next()
})
export default router
