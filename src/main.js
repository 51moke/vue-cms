import Vue from 'vue'
import vue51moke, { App } from 'vue51moke'

// import axios from 'axios'
// import App from './App.vue'
// import ElementUi from 'element-ui'
import store from './store'
import router from './router'
import layout from './layout'
import i18n from './lang'

import './styles/base.scss'

import './registerServiceWorker'
// console.log('ElementUi', ElementUi)
// console.log('VueRouter', VueRouter)

// 导航守卫demo
// router.beforeEach((to, from, next) => {
//   console.log('导航拦截：', to, from)
//   next()
// })

Vue.use(vue51moke, {
  // api: {},
  // defaultLayout: {
  //   template: `<div class="mo-ren">默认布局<slot></slot></div>`
  // },
  json: {},
  pages: {},
  frameLayouts: layout
})

Vue.config.productionTip = false

new Vue({
  store,
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')
