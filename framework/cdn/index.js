
const { Vue, ELEMENT, VueRouter, Vuex, axios, VueI18n, Cookies } = window

window.Vue = undefined
window.ELEMENT = undefined
window.VueRouter = undefined
window.Vuex = undefined
window.axios = undefined
window.VueI18n = undefined
window.Cookies = undefined

export { Vue, ELEMENT, VueRouter, Vuex, axios, VueI18n, Cookies }

// console.log('cdn处理')
