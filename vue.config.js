
const path = require('path')

function resolve (dir) {
  return path.join(__dirname, '.', dir)
}

// 打包目录
// console.log('env', process.env.OUTPUT_DIR)

module.exports = {
  outputDir: process.env.OUTPUT_DIR || 'dist',
  css: {
    // 是否开启支持 foo.module.css 样式
    requireModuleExtension: true,
    // 是否使用 css 分离插件 ExtractTextPlugin，采用独立样式文件载入，不采用<style>     方式内联至 html 文件中
    extract: true
  },
  // 静态路径
  publicPath: '',
  configureWebpack: (c) => {
    // delete c.externals.index
    // console.log('配置', c)
    return {
      entry: ['./framework/cdn/index.js', './src/main.js'],
      // externals: {
      //   'element-ui': 'element-ui'
      // },
      // externals: {
      //   'element-ui': 'element-ui',
      //   vue: 'Vue', // 左侧vue是我们自己引入时候要用的，右侧是开发依赖库的主人定义的不能修改
      //   'vue-router': 'VueRouter',
      //   vuex: 'Vuex',
      //   axios: 'axios',
      //   'mint-ui': 'MINT',
      //   'crypto-js': 'CryptoJS'
      // },
      resolve: {
        extensions: ['.js', '.vue', '.json'],
        alias: {
          // 添加vue别名，以cdn引入
          vue$: resolve('framework/cdn/vue.js'),
          vuex$: resolve('framework/cdn/vuex.js'),
          axios$: resolve('framework/cdn/axios.js'),
          'element-ui$': resolve('framework/cdn/element-ui.js'),
          'element-ui/lib/locale$': resolve('framework/cdn/element-ui.locale.js'),
          'element-ui/lib/locale/lang/zh-CN$': resolve('framework/cdn/element-ui.locale.lang.zh-CN.js'),
          'element-ui/lib/locale/lang/en$': resolve('framework/cdn/element-ui.locale.lang.en.js'),
          'vue-router$': resolve('framework/cdn/vue-router.js'),
          'vue-i18n$': resolve('framework/cdn/vue-i18n.js'),
          'js-cookie$': resolve('framework/cdn/js-cookie.js'),
          assets: '@/assets',
          '@pages': '@/pages',
          '@components': '@/components'
        }
      }
    }
  },
  devServer: {
    proxy: {
      '/api': {
        target: 'http://cms.51moke.com/', // 目标地址
        changeOrigin: true // 开启代理：在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样服务端和服务端进行数据的交互就不会有跨域问题

      }

    }
  }

}
